import 'package:flutter/material.dart';

/// Header size
enum H { h1, h2, h3, h4, h5, h6 }

/// Bold font
enum B { b1, b2, b3, b4, b5 }

/// Font Style
enum S { roboto, breeSerif }

/// Text Styles
class TextStyles {
  /// select style for your text
  static TextStyle style(H textSize, B boldFont, S fontStyle, Color color) {
    double localTextSize;
    FontWeight localBoldFont;
    String localFontStyle;

    switch (textSize) {
      case H.h1:
        localTextSize = 40;
        break;
      case H.h2:
        localTextSize = 32;
        break;
      case H.h3:
        localTextSize = 24;
        break;
      case H.h4:
        localTextSize = 16;
        break;
      case H.h5:
        localTextSize = 12;
        break;
      default:
        localTextSize = 10;
    }

    switch (boldFont) {
      case B.b1:
        localBoldFont = FontWeight.w100;
        break;
      case B.b2:
        localBoldFont = FontWeight.w200;
        break;
      case B.b3:
        localBoldFont = FontWeight.w500;
        break;
      case B.b4:
        localBoldFont = FontWeight.w700;
        break;
      default:
        localBoldFont = FontWeight.w900;
    }

    switch (fontStyle) {
      case S.roboto:
        localFontStyle = 'Roboto';
        break;
      case S.breeSerif:
        localFontStyle = 'BreeSeriff';
        break;
      default:
        localFontStyle = 'BreeSeriff';
    }

    return TextStyle(
      fontSize: localTextSize,
      fontWeight: localBoldFont,
      fontFamily: localFontStyle,
    );
  }
}
